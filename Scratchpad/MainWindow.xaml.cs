﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

using System;
using System.Drawing;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using GalaSoft.MvvmLight.Messaging;
using Scratchpad.Extensions;
using Scratchpad.Utilities;
using Scratchpad.ViewModels;
using Application = System.Windows.Application;

namespace Scratchpad
{
    public partial class MainWindow
    {
        private const int HotkeyId = 5000;
        private readonly NotifyIcon _notifyIcon;

        private readonly PadViewModel _viewModel = new PadViewModel();

        private HwndSource _source;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = _viewModel;

            _notifyIcon = new NotifyIcon();
            _notifyIcon.Click += NotifyIcon_Click;
            var iconStream = Application.GetResourceStream(new Uri("pack://application:,,,/icon.ico")).Stream;
            _notifyIcon.Icon = new Icon(iconStream);
            iconStream.Dispose();

            SetWindowPosition();
            PreRenderWindow();

            RegisterViewModelMessenger();
        }

        /// <summary>
        ///     This prevent eventual artifact/lag/... when first showing the window
        /// </summary>
        private void PreRenderWindow()
        {
            Opacity = 0;
            Show();
            Hide();
            Opacity = 1;
        }

        private void RegisterViewModelMessenger()
        {
            Messenger.Default.Register<Nothing>(this, PadViewModel.FocusAndSelect, FocusAndSelectText);
        }

        /// <summary>
        ///     Adapt to eventual theme change in user's session
        /// </summary>
        private void UpdateTheme()
        {
            ThemeUtils.UpdateThemeResources(Application.Current.Resources);
            if (ThemeUtils.IsWindowTransparencyEnabled)
                this.EnableBlur();
            else
                this.DisableBlur();
        }

        /// <summary>
        ///     Update the window position according to the taskbar
        ///     TODO: handle other taskbar position
        /// </summary>
        private void SetWindowPosition()
        {
            var desktopArea = SystemParameters.WorkArea;
            Left = desktopArea.Right - Width;
            Top = desktopArea.Bottom - Height;
        }

        private void FocusAndSelectText(Nothing _)
        {
            Pad.Focus();
            Pad.SelectAll();
        }

        private void RegisterHotKey()
        {
            _source = this.BindHotKey(HotkeyId,
                GlobalHotKeyExtensions.ModAlt | GlobalHotKeyExtensions.ModCtrl | GlobalHotKeyExtensions.ModShift,
                _source, HwndHook);
        }

        private IntPtr HwndHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case GlobalHotKeyExtensions.WmHotkey:
                    switch (wParam.ToInt32())
                    {
                        case HotkeyId:
                            var vkey = ((int) lParam >> 16) & 0xFFFF;
                            if (vkey == 0x53)
                                ShowHidePad();
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        private void ShowHidePad()
        {
            if (IsVisible)
            {
                this.HideWithAnimation();
            }
            else
            {
                UpdateTheme();
                SetWindowPosition();
                FocusAndSelectText(Nothing.AtAll);
                this.ShowWithAnimation();
            }
        }

        private void NotifyIcon_Click(object sender, EventArgs e)
        {
            ShowHidePad();
        }

        private void Window_Deactivated(object sender, EventArgs e)
        {
            this.HideWithAnimation();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _notifyIcon.Visible = true;
        }

        private void Window_SourceInitialized(object sender, EventArgs e)
        {
            RegisterHotKey();
        }

        private void Pad_DelayedTextChanged(object sender, EventArgs e)
        {
            _viewModel.Save();
        }
    }
}