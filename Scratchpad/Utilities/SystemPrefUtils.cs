﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

using Microsoft.Win32;

namespace Scratchpad.Utilities
{
    internal static class SystemPrefUtils
    {
        /// <summary>
        ///     Whether the user has enabled transparency in Windows settings
        /// </summary>
        public static bool IsTransparencyEnabled
        {
            get
            {
                using (var baseKey = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64))
                {
                    // ReSharper disable once PossibleNullReferenceException
                    return (int) baseKey.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Themes\Personalize")
                               .GetValue("EnableTransparency", 0) > 0;
                }
            }
        }

        /// <summary>
        ///     Whether the user has enabled accent color for the taskbar in Windows settings
        /// </summary>
        public static bool UseAccentColor
        {
            get
            {
                using (var baseKey = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64))
                {
                    // ReSharper disable once PossibleNullReferenceException
                    return (int) baseKey.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Themes\Personalize")
                               .GetValue("ColorPrevalence", 0) > 0;
                }
            }
        }
    }
}