﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

using System;
using System.IO;

namespace Scratchpad.Utilities
{
    internal static class StorageUtils
    {
        private const string AppDirectoryName = "Scratchpad";
        private const string ScratchDirectoryName = "scratches";

        /// <summary>
        ///     Get the root data directory
        /// </summary>
        /// <returns>The path to the root data directory</returns>
        public static string GetDataStorage()
        {
            var appDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                         Path.DirectorySeparatorChar + AppDirectoryName;

            if (Directory.Exists(appDir)) return appDir;

            try
            {
                Directory.CreateDirectory(appDir);
            }
            catch (Exception)
            {
                return null;
            }

            return appDir;
        }

        /// <summary>
        ///     Get the scratches directory
        /// </summary>
        /// <returns>The path to the scratches directory</returns>
        public static string GetScratchesDirectories()
        {
            var dir = GetDataStorage() + Path.DirectorySeparatorChar + ScratchDirectoryName;
            if (Directory.Exists(dir)) return dir;

            try
            {
                Directory.CreateDirectory(dir);
            }
            catch (Exception)
            {
                return null;
            }

            return dir;
        }

        /// <summary>
        ///     Write data to the specified file
        /// </summary>
        /// <param name="filepath">Absolute path to the file</param>
        /// <param name="text">Data to write</param>
        public static void WriteFile(string filepath, string text)
        {
            File.WriteAllText(filepath, text);
        }
    }
}