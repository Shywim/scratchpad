﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

using System.Collections;
using System.Windows;
using System.Windows.Media;

namespace Scratchpad.Utilities
{
    public static class ThemeUtils
    {
        public static bool IsWindowTransparencyEnabled =>
            !SystemParameters.HighContrast && SystemPrefUtils.IsTransparencyEnabled;

        public static void UpdateThemeResources(ResourceDictionary dictionary)
        {
            dictionary["WindowBackground"] = new SolidColorBrush(GetWindowBackgroundColor());

            ReplaceBrush(dictionary, "WindowForeground", "ImmersiveApplicationTextDarkTheme", 1.0);
            ReplaceBrush(dictionary, "AccentBrush", "ImmersiveSystemAccent", 0.2);
            ReplaceBrush(dictionary, "AccentLight1", "ImmersiveSystemAccentLight1", 1.0);
            ReplaceBrush(dictionary, "AccentBrushHighlight", "ImmersiveSystemAccent", 1.0);
        }

        private static Color GetWindowBackgroundColor()
        {
            string resource;
            if (SystemParameters.HighContrast)
                resource = "ImmersiveApplicationBackground";
            else if (SystemPrefUtils.UseAccentColor)
                resource = IsWindowTransparencyEnabled ? "ImmersiveSystemAccentDark2" : "ImmersiveSystemAccentDark1";
            else
                resource = "ImmersiveDarkChromeMedium";

            var color = AccentColorService.GetColorByTypeName(resource);
            color.A = (byte) (IsWindowTransparencyEnabled ? 190 : 255);
            return color;
        }

        private static void SetBrush(IDictionary dictionary, string name, string immersiveAccentName,
            double opacity = 1.0)
        {
            if (dictionary[name] == null)
                ReplaceBrush(dictionary, name, immersiveAccentName, opacity);
            else
                AlterBrush(dictionary, name, immersiveAccentName, opacity);
        }

        private static void AlterBrush(IDictionary dictionary, string name, string immersiveAccentName,
            double opacity)
        {
            var color = AccentColorService.GetColorByTypeName(immersiveAccentName);
            color.A = (byte) (opacity * 255);
            ((SolidColorBrush) dictionary[name]).Color = color;
        }

        private static void ReplaceBrush(IDictionary dictionary, string name,
            string immersiveAccentName, double opacity)
        {
            var color = AccentColorService.GetColorByTypeName(immersiveAccentName);
            color.A = (byte) (opacity * 255);
            dictionary[name] = new SolidColorBrush(color);
        }
    }
}