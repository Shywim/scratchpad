﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

using System;
using System.Runtime.InteropServices;
using System.Windows.Media;

namespace Scratchpad.Utilities
{
    /// <summary>
    ///     From https://www.quppa.net/blog/2013/01/02/retrieving-windows-8-theme-colours/
    /// </summary>
    public static class AccentColorService
    {
        public static Color GetColorByTypeName(string name)
        {
            var colorSet = Interop.GetImmersiveUserColorSetPreference(false, false);
            var colorType = Interop.GetImmersiveColorTypeFromName(name);

            var rawColor = Interop.GetImmersiveColorFromColorSetEx(colorSet, colorType, false, 0);

            var colorBytes = new byte[4];
            colorBytes[0] = (byte) ((0xFF000000 & rawColor) >> 24); // A
            colorBytes[1] = (byte) ((0x00FF0000 & rawColor) >> 16); // B
            colorBytes[2] = (byte) ((0x0000FF00 & rawColor) >> 8); // G
            colorBytes[3] = (byte) (0x000000FF & rawColor); // R

            return Color.FromArgb(colorBytes[0], colorBytes[3], colorBytes[2], colorBytes[1]);
        }

        private static class Interop
        {
            [DllImport("uxtheme.dll", EntryPoint = "#94", CharSet = CharSet.Unicode)]
            internal static extern int GetImmersiveColorSetCount();

            /// <summary>
            ///     Retrieves an immersive colour from the specified colour set.
            /// </summary>
            /// <param name="dwImmersiveColorSet">
            ///     Colour set index. Use <see cref="GetImmersiveColorSetCount" /> to get the number of
            ///     colour sets available.
            /// </param>
            /// <param name="dwImmersiveColorType">
            ///     The colour type. Use <see cref="GetImmersiveColorTypeFromName" /> to get the type
            ///     from an element name.
            /// </param>
            /// <param name="bIgnoreHighContrast">
            ///     Set this to true to return colours from the current colour set, even if a high
            ///     contrast theme is being used.
            /// </param>
            /// <param name="dwHighContrastCacheMode">
            ///     Set this to 1 to force UxTheme to check whether high contrast mode is enabled. If
            ///     this is set to 0, UxTheme will only perform this check if high contrast mode is currently disabled.
            /// </param>
            /// <returns>Returns a colour (0xAABBGGRR).</returns>
            [DllImport("uxtheme.dll", EntryPoint = "#95", CharSet = CharSet.Unicode)]
            internal static extern uint GetImmersiveColorFromColorSetEx(uint dwImmersiveColorSet,
                uint dwImmersiveColorType, bool bIgnoreHighContrast, uint dwHighContrastCacheMode);

            /// <summary>
            ///     Retrieves an immersive colour type given its name.
            /// </summary>
            /// <param name="name">Pointer to a string containing the name preprended with 9 characters (e.g. "Immersive" + name).</param>
            /// <returns>Colour type.</returns>
            [DllImport("uxtheme.dll", EntryPoint = "#96", CharSet = CharSet.Unicode)]
            internal static extern uint GetImmersiveColorTypeFromName(string name);

            /// <summary>
            ///     Gets the user's colour set preference (or default colour set if the user isn't allowed to modify this setting
            ///     according to group policy).
            /// </summary>
            /// <param name="bForceCheckRegistry">
            ///     Forces update from registry
            ///     (HKCUSoftwareMicrosoftWindowsCurrentVersionExplorerAccentColorSet_Version3).
            /// </param>
            /// <param name="bSkipCheckOnFail">Skip second check if first result is -1.</param>
            /// <returns>User colour set preference.</returns>
            [DllImport("uxtheme.dll", EntryPoint = "#98", CharSet = CharSet.Unicode)]
            internal static extern uint GetImmersiveUserColorSetPreference(bool bForceCheckRegistry,
                bool bSkipCheckOnFail);

            /// <summary>
            ///     Retrieves names of colour types by index.
            /// </summary>
            /// <param name="dwIndex">Colour type index (0 to 766/0x2fe).</param>
            /// <returns>Pointer to a string containing the colour type's name.</returns>
            [DllImport("uxtheme.dll", EntryPoint = "#100", CharSet = CharSet.Unicode)]
            internal static extern IntPtr GetImmersiveColorNamedTypeByIndex(uint dwIndex);
        }
    }
}