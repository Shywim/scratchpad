﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Scratchpad.Extensions
{
    internal static class WindowExtensions
    {
        private static bool _hideAnimationInProgress;

        private static bool _showAnimationInProgress;

        public static void HideWithAnimation(this Window window)
        {
            if (_hideAnimationInProgress) return;

            try
            {
                _hideAnimationInProgress = true;

                var hideAnimation = new DoubleAnimation
                {
                    Duration = new Duration(TimeSpan.FromSeconds(0.2)),
                    FillBehavior = FillBehavior.Stop,
                    EasingFunction = new ExponentialEase {EasingMode = EasingMode.EaseIn},
                    From = window.Top
                };


                hideAnimation.To = hideAnimation.From + 20;
                hideAnimation.Completed += (s, e) =>
                {
                    window.Visibility = Visibility.Hidden;
                    _hideAnimationInProgress = false;
                };

                window.ApplyAnimationClock(Window.TopProperty, hideAnimation.CreateClock());
            }
            catch
            {
                _hideAnimationInProgress = false;
            }
        }

        public static void ShowWithAnimation(this Window window)
        {
            if (_showAnimationInProgress) return;

            try
            {
                _showAnimationInProgress = true;
                var showAnimation = new DoubleAnimation
                {
                    Duration = new Duration(TimeSpan.FromSeconds(0.3)),
                    FillBehavior = FillBehavior.Stop,
                    EasingFunction = new ExponentialEase {EasingMode = EasingMode.EaseOut},
                    To = window.Top
                };


                showAnimation.From = showAnimation.To + 45;
                showAnimation.Completed += (s, e) =>
                {
                    window.Topmost = true;
                    _showAnimationInProgress = false;
                    window.Focus();
                };

                window.Visibility = Visibility.Visible;
                window.Topmost = false;
                window.Activate();

                window.ApplyAnimationClock(Window.TopProperty, showAnimation.CreateClock());
            }
            catch
            {
                _showAnimationInProgress = false;
            }
        }

        public static Matrix CalculateDpiFactors(this Window window)
        {
            var mainWindowPresentationSource = PresentationSource.FromVisual(window);
            return mainWindowPresentationSource?.CompositionTarget?.TransformToDevice ?? new Matrix {M11 = 1, M22 = 1};
        }

        public static double DpiHeightFactor(this Window window)
        {
            var m = CalculateDpiFactors(window);
            return m.M22;
        }

        public static double DpiWidthFactor(this Window window)
        {
            var m = CalculateDpiFactors(window);
            return m.M11;
        }
    }
}