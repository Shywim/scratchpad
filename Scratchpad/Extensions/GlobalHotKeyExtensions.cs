﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace Scratchpad.Extensions
{
    internal static class GlobalHotKeyExtensions
    {
        public const int WmHotkey = 0x0312;

        public const uint ModNone = 0x0000;
        public const uint ModAlt = 0x0001;
        public const uint ModCtrl = 0x0002;
        public const uint ModShift = 0x0004;
        public const uint ModWin = 0x0008;

        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        internal static HwndSource BindHotKey(this Window window, int id, uint modifiers, HwndSource source,
            HwndSourceHook callback)
        {
            IntPtr handle;

            if (source == null)
            {
                handle = new WindowInteropHelper(window).Handle;
                source = HwndSource.FromHwnd(handle);
                source?.AddHook(callback);
            }
            else
            {
                handle = source.Handle;
            }

            RegisterHotKey(handle, id, modifiers, 0x53);

            return source;
        }
    }
}