﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Scratchpad.Models;
using Scratchpad.Utilities;
using Scratchpad.ViewModels.Commands;

namespace Scratchpad.ViewModels
{
    internal class PadViewModel : ViewModelBase
    {
        public const string FocusAndSelect = "FocusAndSelect";
        private ICommand _newScratch;

        private Scratch _scratch;

        private Scratch[] _scratches;

        public PadViewModel()
        {
            RefreshRecentScratches();
            RestoreLastScratch();
        }

        public List<Scratch> RecentScratches
        {
            get
            {
                var sc = _scratches.OrderByDescending(s => s.Name).ToList();
                return sc;
            }
            set
            {
                _scratches = value.ToArray();
                RaisePropertyChanged();
            }
        }

        public ICommand NewScratch
        {
            get
            {
                return _newScratch
                       ?? (_newScratch = new ActionCommand(() =>
                       {
                           Scratch?.Save();
                           Scratch = new Scratch();

                           var recent = RecentScratches;
                           recent.Add(Scratch);
                           RecentScratches = new List<Scratch>(recent);

                           Messenger.Default.Send(Nothing.AtAll, FocusAndSelect);
                       }));
            }
        }

        public Scratch Scratch
        {
            get => _scratch;
            set
            {
                _scratch?.Save();
                _scratch = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Get the last 5 scratches from disk
        /// </summary>
        private void RefreshRecentScratches()
        {
            var files = Directory.EnumerateFiles(StorageUtils.GetScratchesDirectories(), "scratch-*");
            var filesList = files as IList<string> ?? files.ToList().OrderByDescending(Path.GetFileName).Take(5);

            var scratches = new List<Scratch>();
            foreach (var file in filesList)
            {
                var content = File.ReadAllText(file);
                var filename = Path.GetFileName(file);
                scratches.Add(new Scratch(filename, filename.Contains("-pinned"), content));
            }

            RecentScratches = scratches;
        }

        /// <summary>
        ///     Restore the most recent scratch
        /// </summary>
        private void RestoreLastScratch()
        {
            Scratch = RecentScratches.FirstOrDefault();
        }

        public void Save()
        {
            Scratch?.Save();
            RefreshRecentScratches();
        }
    }
}