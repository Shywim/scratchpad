﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using Scratchpad.Annotations;
using Scratchpad.Utilities;

namespace Scratchpad.Models
{
    internal class Scratch : INotifyPropertyChanged
    {
        private const string DefaultContent = "What are you thinking?";
        private string _content;

        public Scratch()
        {
            Pinned = false;
            Name = "scratch-" + DateTimeOffset.Now.ToUnixTimeMilliseconds();
            Content = DefaultContent;
        }

        public Scratch(string name, bool pinned, string content)
        {
            Name = name;
            Pinned = pinned;
            Content = content;
        }

        public string Name { get; }

        public string Content
        {
            get => _content;
            set
            {
                _content = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Whether the scratch is pinned (e.g. prevented from deletion) or not
        /// </summary>
        public bool Pinned { get; }

        private string FileName
        {
            get
            {
                var name = Name;
                if (Pinned)
                    return name + "-pinned";
                return name;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public override bool Equals(object obj)
        {
            var scratch = obj as Scratch;
            return scratch != null &&
                   Name == scratch.Name;
        }

        protected bool Equals(Scratch other)
        {
            return string.Equals(Name, other.Name);
        }

        public override int GetHashCode()
        {
            return Name != null ? Name.GetHashCode() : 0;
        }

        /// <summary>
        ///     Save the scratch to the data directory
        /// </summary>
        public void Save()
        {
            StorageUtils.WriteFile(StorageUtils.GetScratchesDirectories() + Path.DirectorySeparatorChar + FileName,
                Content);
        }

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public override string ToString()
        {
            return Name;
        }
    }
}