﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;

namespace Scratchpad.Controls
{
    public class SyntaxTextEditor : RichTextBox
    {
        private const string IndentPattern = @"^([ \t]+)";

        static SyntaxTextEditor()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SyntaxTextEditor),
                new FrameworkPropertyMetadata(typeof(SyntaxTextEditor)));
        }

        public SyntaxTextEditor()
        {
            DelayedTextChangedTime = 1000;
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                LineBreak();
                e.Handled = true;
                base.OnPreviewKeyDown(e);
            }
            else
            {
                base.OnPreviewKeyDown(e);
            }
        }

        /// <summary>
        ///     Insert a new line respecting indent from previous line
        /// </summary>
        private void LineBreak()
        {
            var pointer = CaretPosition.InsertLineBreak();

            var indent = GetIndent(pointer, -1);
            if (indent != null)
            {
                pointer.InsertTextInRun(indent);
                pointer = pointer.GetPositionAtOffset(indent.Length);
            }

            if (pointer == null) return;

            Selection.Select(pointer, pointer);
        }

        /// <summary>
        ///     Copy indent from last *NOT* empty line
        /// </summary>
        /// <param name="caret">Pointer at the new line</param>
        /// <param name="offset">How many lines to go back</param>
        /// <returns>The previous indent (space and or tabs)</returns>
        [SuppressMessage("ReSharper", "TailRecursiveCall")]
        private static string GetIndent(TextPointer caret, int offset)
        {
            var previousLine = caret.GetLineStartPosition(offset);

            if (previousLine == null) return null;

            var previousLineSelect = new TextRange(previousLine, caret.GetLineStartPosition(offset + 1));
            var previousLineStr = previousLineSelect.Text;

            if (previousLineStr.Equals(Environment.NewLine))
                return GetIndent(caret, offset - 1);

            var indent = Regex.Match(previousLineStr, IndentPattern);

            return indent.Success ? indent.ToString() : null;
        }

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            LaunchDelayedTextChangedEvent();
            base.OnTextChanged(e);
            UpdateTextFromDocument();
        }

        #region DelayedTextChanged

        private Timer _delayedTextChangeTimer;
        public int DelayedTextChangedTime { get; set; }

        public event EventHandler DelayedTextChanged;

        /// <summary>
        ///     Send the DelayedTextChanged after the
        ///     <see cref="DelayedTextChangedTime">
        ///         DelayedTextChangdeTime
        ///     </see>
        ///     delay.
        /// </summary>
        private void LaunchDelayedTextChangedEvent()
        {
            _delayedTextChangeTimer?.Stop();

            if (_delayedTextChangeTimer == null || (int) _delayedTextChangeTimer.Interval != DelayedTextChangedTime)
            {
                _delayedTextChangeTimer = new Timer(DelayedTextChangedTime);
                _delayedTextChangeTimer.Elapsed += OnDelayedTextChangedElapsed;
                _delayedTextChangeTimer.Interval = DelayedTextChangedTime;
            }

            _delayedTextChangeTimer.Start();
        }

        private void OnDelayedTextChangedElapsed(object sender, EventArgs e)
        {
            var timer = sender as Timer;
            timer?.Stop();

            OnDelayedTextChanged(EventArgs.Empty);
        }

        private void OnDelayedTextChanged(EventArgs e)
        {
            DelayedTextChanged?.Invoke(this, e);
        }

        #endregion //DelayedTextChanged

        #region Text

        private bool _preventDocumentUpdate;
        private bool _preventTextUpdate;

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string),
            typeof(RichTextBox),
            new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                OnTextPropertyChanged, CoerceTextProperty, true, UpdateSourceTrigger.LostFocus));

        public string Text
        {
            get => (string) GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        private static void OnTextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((SyntaxTextEditor) d).UpdateDocumentFromText();
        }

        private static object CoerceTextProperty(DependencyObject d, object value)
        {
            return value ?? "";
        }

        private void UpdateTextFromDocument()
        {
            if (_preventTextUpdate)
                return;

            _preventDocumentUpdate = true;
            Text = new TextRange(Document.ContentStart, Document.ContentEnd).Text;
            _preventDocumentUpdate = false;
        }

        private void UpdateDocumentFromText()
        {
            if (_preventDocumentUpdate)
                return;

            _preventTextUpdate = true;
            new TextRange(Document.ContentStart, Document.ContentEnd).Text = Text;
            _preventTextUpdate = false;
        }

        #endregion // Text
    }
}